package com.j2core.maxgls.week2.quadratic;

/**
 * Evaluates roots of quadratic equation
 *
 * @author maxgls
 */
public class QuadraticEquationSolver {

    // I have chosen 'double epsilon' instead of BigDecimal due to
    // there is no need compare two unknown doubles, we will compare with 0
    // Furthermore using primitive double type in that case will increase performance
    private static final double DBL_EPSILON  = 1E-5;

    public Roots solve(int a, int b, int c){
        if (a == 0){
            throw new IllegalArgumentException("First 'a' coefficient can not be zero.");
        }

        final double discriminant = Math.pow(b, 2) - 4 * a * c;

        if (discriminant < -DBL_EPSILON) {
            throw new IllegalArgumentException("This quadratic equation does not have solutions for specified coefficients");
        }
        final long multipleA = 2 * a;

        if (discriminant > DBL_EPSILON) {
            // Calculate two roots
            final double sqrtDiscriminant = Math.sqrt(discriminant);
            final double x1 = (-b + sqrtDiscriminant) / multipleA;
            final double x2 = (-b - sqrtDiscriminant) / multipleA;

            return new Roots(x1, x2);
        }

        // There is only one root if discriminant is zero
        return new Roots(-1D * b / multipleA, null);
    }
}