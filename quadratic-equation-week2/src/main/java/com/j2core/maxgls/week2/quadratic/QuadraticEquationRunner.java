package com.j2core.maxgls.week2.quadratic;

import java.util.Scanner;

/**
 * Entry point to evaluate roots of quadratic equation
 *
 * @author maxgls
 */
public class QuadraticEquationRunner {

    /**
     * Evaluates roots of quadratic equation and print result in output
     *
     * @param args three integer numbers in string array, Console input will be used if parameter is not specified.
     */
    public static void main(String[] args) {
        final int[] coefficients = readCoefficients(args);
        final int a = coefficients[0];
        final int b = coefficients[1];
        final int c = coefficients[2];
        System.out.printf("a=%d b=%d c=%d%n", a, b, c);

        // Find roots of quadratic equation
        final Roots roots = new QuadraticEquationSolver().solve(a, b, c);

        System.out.println("x1 = " + roots.getX1());
        final Double x2 = roots.getX2();
        if (x2 != null){
            System.out.println("x2 = " + roots.getX2());
        }
    }

    private static boolean isInteger(String str) {
        try {
            Integer.parseInt(str);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    private static int[] fromConsole(){
        System.out.println("Please enter coefficient of quadratic equation:");
        final Scanner scaner = new Scanner(System.in);
        final int a = scaner.nextInt();
        final int b = scaner.nextInt();
        final int c = scaner.nextInt();
        scaner.close();

        return new int[] {a,b,c};
    }

    private static int[] fromStringArray(String[] args){
        if (args.length != 3) {
            throw new IllegalArgumentException("The quadratic equation solver requires three integer numbers: a b c");
        }
        final String srtA = args[0];
        final String srtB = args[1];
        final String srtC = args[2];

        if (!isInteger(srtA) ||
                !isInteger(srtB) ||
                !isInteger(srtC)) {
            throw new IllegalArgumentException("All parameters: a,b,c must be an integer.");
        }

        final int a = Integer.parseInt(srtA);
        final int b = Integer.parseInt(srtB);
        final int c = Integer.parseInt(srtC);

        return new int[]{ a, b, c};
    }

    private static int[] readCoefficients(String[] args) {
        final int[] coefficients = args == null || args.length == 0 ?
                fromConsole() :
                fromStringArray(args);
        return coefficients;
    }
}