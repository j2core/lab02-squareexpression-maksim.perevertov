package com.j2core.maxgls.week2.quadratic;

/**
 * Roots of quadratic equation.
 * There is a case when we have only one root (other root is null).
 * That's why x1 and x2 are Double (object)
 *
 * @author maxgls
 */
public class Roots {

    private final Double x1;
    private final Double x2;

    public Roots(Double x1, Double x2){
        this.x1 = x1;
        this.x2 = x2;
    }

    public Double getX1(){
        return this.x1;
    }

    public Double getX2() {
        return this.x2;
    }
}