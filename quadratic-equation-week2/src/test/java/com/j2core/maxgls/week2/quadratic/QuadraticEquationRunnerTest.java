package com.j2core.maxgls.week2.quadratic;

import org.junit.Test;

/**
 * Unit tests for {@link QuadraticEquationRunner}
 *
 * @author maxgls
 */
public class QuadraticEquationRunnerTest {

    @Test(expected = IllegalArgumentException.class)
    public void noRealRootsTest(){
        QuadraticEquationRunner.main(new String[]{"1", "2", "3"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void notEnoughtArgsTest(){
        QuadraticEquationRunner.main(new String[]{"1", "2"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void tooManyAgrsTest(){
        QuadraticEquationRunner.main(new String[]{"0", "1", "2", "3"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void firstArgIsZeroTest(){
        QuadraticEquationRunner.main(new String[]{"0", "2", "3"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void containsDoubleArgNumberTest(){
        QuadraticEquationRunner.main(new String[]{"2", "3", "-4.1"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void containsStringArgTest(){
        QuadraticEquationRunner.main(new String[]{"2", "3", "a"});
    }
}