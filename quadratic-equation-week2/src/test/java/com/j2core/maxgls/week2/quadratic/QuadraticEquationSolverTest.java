package com.j2core.maxgls.week2.quadratic;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit tests for {@link QuadraticEquationSolver}
 *
 * @author maxgls
 */
public class QuadraticEquationSolverTest {

    @Test
    public void twoRootsTest(){
        final Roots roots = new QuadraticEquationSolver().solve(1,-1,-2);
        Assert.assertEquals(2, roots.getX1(), 2);
        Assert.assertEquals(-1, roots.getX2(), 2);
    }

    @Test
    public void oneRootTest() {
        final Roots roots = new QuadraticEquationSolver().solve(1,2,1);
        Assert.assertEquals(-1, roots.getX1(), 2);
        Assert.assertNull(roots.getX2());
    }

    @Test(expected = IllegalArgumentException.class)
    public void noRootsTest() {
        new QuadraticEquationSolver().solve(1,1,1);
    }
}